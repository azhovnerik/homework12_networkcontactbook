import lombok.RequiredArgsConstructor;
import persistence.ContactRepository;
import persistence.NetworkContactRepository;
import services.AuthService;
import services.NetworkAuthService;

import java.util.Properties;
@RequiredArgsConstructor
public class NetworkContactRepositoryFactory implements ContactRepositoryFactory {
    private final Properties properties;
    @Override
    public ContactRepository createContactRepositort() {
        NetworkAuthService as  = createAuthService();
        return new NetworkContactRepository(as);
    }
    private NetworkAuthService networkAuthService;

    @Override
    public NetworkAuthService createAuthService() {
        if (networkAuthService==null){
            networkAuthService = new NetworkAuthService();
        }
        return networkAuthService;
    }
}
