package dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import entities.Contact;
import entities.TypeContact;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Data
@Setter
@RequiredArgsConstructor
public class ContactDto implements Serializable {

    @JsonProperty("id")
    private String uuid;
    private String name;
    private TypeContactDto type;
    private String value;
   // private String fullName;

    public ContactDto( String name,  TypeContactDto type, String value) {

        this.name = name;


        this.type = type;
        this.value = value;
        this.uuid = UUID.randomUUID().toString();
    }



    @Override
    public String toString() {
        return  name+":"+ type.getName()+value+":"+uuid;
    }


}
