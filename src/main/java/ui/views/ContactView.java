package ui.views;

import entities.Contact;
import entities.User;


import java.util.List;

public interface ContactView {
    Contact readContact();
    User readNewUser();
    User readUserForLogin();
    void showContacts(List<Contact> lst);
    void showUsers(List<User> lst);

}
