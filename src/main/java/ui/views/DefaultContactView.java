package ui.views;

import entities.Contact;
import entities.TypeContact;
import entities.User;

import lombok.RequiredArgsConstructor;
import services.ContactService;

import java.util.List;
import java.util.Scanner;
@RequiredArgsConstructor
public class DefaultContactView implements ContactView {
    private final Scanner sc;
    @Override
    public Contact readContact() {
        String surname;
        String name;
        String patronomic ="";
        TypeContact type;
        String value;
         /*String email ="";
         String phone="";*/

        System.out.println(" введите фамилию , имя и отчество нового контакта");
        System.out.print("surname:");
       // Scanner sc = new Scanner(System.in);
        surname = sc.nextLine();
        System.out.print("name:");
        //sc = new Scanner(System.in);
        name = sc.nextLine();
        System.out.print("patronomic:");
        //sc = new Scanner(System.in);
        patronomic = sc.nextLine();
        TypeContact[] listTypeContact = TypeContact.values();
        System.out.println(" выберите тип контакта");
        for (int i = 0; i < listTypeContact.length ; i++) {
            System.out.printf("%2d - %s\n",i+1,listTypeContact[i].name());
        }
        System.out.print("TypeContact:");
        int numberOfTypeContact = sc.nextInt();
        sc.nextLine();
        TypeContact typeContact = TypeContact.values()[numberOfTypeContact-1];
        type = typeContact;
        //ввод контакта с проверкой валидности
       // while (true) {
            System.out.println(" введите значение контакта");
            System.out.print(typeContact + ":");
            value = sc.nextLine();
           /* if (ContactService.isValid(value,typeContact)){
                break;}
            else{
                System.out.println("НЕВАЛИДНЫЙ "+typeContact+".Повторите ввод");
            }*/

        // uuid = UUID.randomUUID().toString();
        return new Contact(surname,name,patronomic,type,value);
    }

    @Override
    public User readNewUser() {
        String login;
        String password;
        String dateBorn;
         /*String email ="";
         String phone="";*/

        System.out.println(" введите логин, пароль и дату рождения нового юзера");
        System.out.print("login:");
        // Scanner sc = new Scanner(System.in);
        login = sc.nextLine();
        System.out.print("password:");
        //sc = new Scanner(System.in);
        password = sc.nextLine();
        System.out.print("date_born:");
        //sc = new Scanner(System.in);
        dateBorn = sc.nextLine();

      //  return new User(login,dateBorn,password);
       return new User(login,dateBorn,password);
    }

    @Override
    public User readUserForLogin() {
        String login;
        String password;

         /*String email ="";
         String phone="";*/

        System.out.println(" введите логин, пароль для входа в систему");
        System.out.print("login:");
        // Scanner sc = new Scanner(System.in);
        login = sc.nextLine();
        System.out.print("password:");
        //sc = new Scanner(System.in);
        password = sc.nextLine();
               //  return new User(login,dateBorn,password);
        return new User(login,password);
    }

    @Override
    public void showContacts(List<Contact> lst){
        for (Contact cnt:lst) {
            System.out.println(cnt);
        }
    }

    @Override
    public void showUsers(List<User> lst) {
        for (User usr:lst) {
            System.out.println(usr);
        }
    }
}
