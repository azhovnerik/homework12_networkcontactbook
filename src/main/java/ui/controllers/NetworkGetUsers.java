package ui.controllers;

import lombok.RequiredArgsConstructor;
import persistence.ContactRepository;
import persistence.NetworkContactRepository;
import services.ContactService;
import services.MenuItem;
import services.NetworkAuthService;
import ui.views.ContactView;
@RequiredArgsConstructor
public class NetworkGetUsers implements MenuItem {
    private final ContactService contactService;
    private final ContactView contactView;
    @Override
    public String getName() {
        return "Network repository - get users";
    }

    @Override
    public void execute() {
        contactView.showUsers(contactService.SelectAllUsers());


    }

    @Override
    public boolean isVisible() {
        ContactRepository contactRepository  = contactService.getContactRepository();
        if (contactRepository instanceof NetworkContactRepository){

                return true;

        }
        return false;
    }
}
