package ui.controllers;

import entities.Contact;
import persistence.ContactRepository;
import persistence.NetworkContactRepository;
import services.ContactService;
import services.MenuItem;
import services.NetworkAuthService;
import ui.views.ContactView;

import java.util.List;

public class SelectContactsWithSorting implements MenuItem {
    //private ArrayList<Contact> contactList;
    private final ContactService contactService;
    private final ContactView contactView;

    public SelectContactsWithSorting(ContactService contactService, ContactView contactView) {
        this.contactService = contactService;
        this.contactView = contactView;
    }




    @Override
    public String getName() {
        return "Select contacts with sorting";
    }

    @Override
    public void execute() {

        contactView.showContacts(contactService.SelectContactsWithSorting());


    }

    @Override
    public boolean isVisible() {
        ContactRepository contactRepository  = contactService.getContactRepository();
        if (contactRepository instanceof NetworkContactRepository){
            NetworkAuthService as = (((NetworkContactRepository) contactRepository).getAuthService());
            if (as.isAuth()) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }
}
