package ui.controllers;

import entities.User;
import lombok.RequiredArgsConstructor;
import persistence.ContactRepository;
import persistence.NetworkContactRepository;
import services.AuthService;
import services.ContactService;
import services.MenuItem;
import ui.views.ContactView;

@RequiredArgsConstructor
public class NetworkLogin implements MenuItem {
    private final ContactService contactService;
    private final ContactView contactView;
   // private final AuthService authService;
    @Override
    public String getName() {
        return "Network repository - login";
    }

    @Override
    public void execute() {
        User user = contactView.readUserForLogin();
        NetworkContactRepository networkContactRepository = (NetworkContactRepository) contactService.getContactRepository();
        //authService.login(user.getLogin(),user.getPassword());
        networkContactRepository.getAuthService().login(user.getLogin(),user.getPassword());


    }
    @Override
    public boolean isVisible() {
        ContactRepository contactRepository  = contactService.getContactRepository();
        if (contactRepository instanceof NetworkContactRepository){

            return true;

        }
        return false;
    }
}
