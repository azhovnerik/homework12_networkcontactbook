package ui.controllers;

import entities.Contact;
import entities.TypeContact;
import exception.ValidateException;
import persistence.ContactRepository;
import persistence.NetworkContactRepository;
import services.ContactService;
import services.MenuItem;
import services.NetworkAuthService;
import ui.views.ContactView;
import ui.views.DefaultContactView;

import java.util.Scanner;
import java.util.UUID;

public class AddContactMenuItem implements MenuItem {
    //private ArrayList<Contact> contactList;
    private final ContactService contactService;
    private final ContactView contactView;
    Scanner scanner;

    public AddContactMenuItem(ContactService contactService, Scanner scanner, ContactView contactView) {
        //this.contactList = contactList;
        this.contactService = contactService;
        this.scanner = scanner;
        this.contactView = contactView;
    }

    @Override
    public String getName() {
        return "Add contact";
    }

    @Override
    public void execute() {
       // contactList.add(addNewContact());
        try {
            contactService.addContact(contactView.readContact());
        }catch (ValidateException exception){
            System.out.println(exception.getMessage());
        }
    }

    @Override
    public boolean isVisible() {
        ContactRepository contactRepository  = contactService.getContactRepository();
        if (contactRepository instanceof NetworkContactRepository){
            NetworkAuthService as = (((NetworkContactRepository) contactRepository).getAuthService());
            if (as.isAuth()) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }


}
