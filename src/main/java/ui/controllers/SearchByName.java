package ui.controllers;

import entities.Contact;
import persistence.ContactRepository;
import persistence.NetworkContactRepository;
import services.ContactService;
import services.MenuItem;
import services.NetworkAuthService;
import ui.views.ContactView;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class SearchByName implements MenuItem {
    private final ContactService contactService;
    private final ContactView contactView;
    private Scanner sc ;

    public SearchByName(ContactService contactService,ContactView contactView, Scanner sc) {
        this.contactService = contactService;
        this.sc = sc;
        this.contactView =contactView;
    }


    @Override
    public String getName() {
        return "Search by name (part of name)";
    }

    @Override
    public void execute() {
        System.out.print(" Введите часть имени:");
        String substr = sc.nextLine();
        contactView.showContacts(contactService.searchByName(substr));
    }



    @Override
    public boolean isVisible() {
        ContactRepository contactRepository  = contactService.getContactRepository();
        if (contactRepository instanceof NetworkContactRepository){
            NetworkAuthService as = (((NetworkContactRepository) contactRepository).getAuthService());
            if (as.isAuth()) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }
}
