package ui.controllers;

import lombok.RequiredArgsConstructor;
import persistence.ContactRepository;
import persistence.NetworkContactRepository;
import services.ContactService;
import services.MenuItem;
import services.NetworkAuthService;
import ui.views.ContactView;

@RequiredArgsConstructor
public class SelectOnlyEmails implements MenuItem {
    private final ContactService contactService;
    private final ContactView contactView;

  /*  public SelectOnlyEmails(ContactService contactService) {
        this.contactService = contactService;
    }*/

    @Override
    public String getName() {
        return "Select only emails";
    }

    @Override
    public void execute() {
       contactView.showContacts(contactService.SelectByEmail());

    }

    @Override
    public boolean isVisible() {
        ContactRepository contactRepository  = contactService.getContactRepository();
        if (contactRepository instanceof NetworkContactRepository){
            return false;

        }
        return true;
    }
}
