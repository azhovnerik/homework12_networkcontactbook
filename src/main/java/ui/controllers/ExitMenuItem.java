package ui.controllers;

import services.MenuItem;

public class ExitMenuItem implements MenuItem {
    @Override
    public String getName() {
        return "Exit";
    }

    @Override
    public void execute() {

    }

    @Override
    public boolean isFinal() {
        return true;
    }
}
