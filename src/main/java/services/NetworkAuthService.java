package services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import entities.User;
import exception.DuplicateContactException;
import exception.DuplicateUserException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import persistence.NetworkContactRepository;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class NetworkAuthService implements AuthService {
    HttpClient httpClient = HttpClient.newHttpClient();
    @Getter
    private  String token;
    @Override
    public void login(String login, String pass) {
    //реализовать
        //HttpClient httpClient = HttpClient.newHttpClient();
        /*HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create("http://miraggio.ua/api/v1/orders/search/?create_date=2022-01-04&accuracy=begin"))
                .header("API-key", "7a47e74e13c9eaa6acd7f4687c58b9c1a70e25152b4b4689ba51c8436bba89d7")
                .header("Accept", "application/json")
                .GET()
                .build();*/

        //создадим мапу с логином и паролем авторизации
        Map<String,String> creds = new HashMap<>() ;
        creds.put("login",login);
        creds.put("password",pass);

        //сконвертим мапу в json
        ObjectMapper objectMapper = new ObjectMapper();
        String requestBody="";
        try {
             requestBody = objectMapper
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(creds);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(NetworkContactRepository.HOST+"/login"))
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();
        try{
            HttpResponse<String> response = httpClient.send(httpRequest,HttpResponse.BodyHandlers.ofString());
            System.out.println(response.statusCode());
            System.out.println(response.headers());
            System.out.println(response.body());
            Gson g = new Gson();
            //Type type = new TypeToken<Map<String, Seanse>>(){}.getType();
            Map<String, String> mapResponceBody = g.fromJson(response.body().toString(), Map.class);
            token = mapResponceBody.get("token");

        }catch (IOException |InterruptedException e){
            e.printStackTrace();
        }

    }

    @Override
    public void register(String login, String dataBorn, String pass) {
        Map<String,String> creds = new HashMap<>() ;
        creds.put("login",login);
        creds.put("password",pass);
        creds.put("date_born",dataBorn);

        //сконвертим мапу в json
        ObjectMapper objectMapper = new ObjectMapper();
        String requestBody="";
        try {
            requestBody = objectMapper
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(creds);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(NetworkContactRepository.HOST+"/register"))
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();
        try{
            HttpResponse<String> response = httpClient.send(httpRequest,HttpResponse.BodyHandlers.ofString());
            System.out.println(response.statusCode());
            System.out.println(response.headers());
            System.out.println(response.body());
            Gson g = new Gson();
            //Type type = new TypeToken<Map<String, Seanse>>(){}.getType();
            Map<String, String> mapResponceBody = g.fromJson(response.body().toString(), Map.class);
            String status = mapResponceBody.get("status");
            if (status.equals("ok")){
                System.out.println("Зарегистрирован новый пользователь: "+login+" !");
            }else {
                throw new DuplicateUserException(mapResponceBody.get("error"));
            }

        }catch (IOException |InterruptedException e){
            e.printStackTrace();
        }
    }




    @Override
    public boolean isAuth() {
        return token !=null;
    }
}
