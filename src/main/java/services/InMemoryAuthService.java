package services;

public class InMemoryAuthService implements AuthService{
    private  final static  String LOGIN = "admin";
    private  final static  String PASS = "admin";
    private  boolean auth = false;

    @Override
    public void login(String login, String pass) {
        if (!login.equals(LOGIN)||!pass.equals(PASS)){
            throw new IllegalArgumentException();
        }
        auth =true;
    }

    @Override
    public void register(String login, String dataBorn, String pass) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isAuth() {
        return auth;
    }


}
