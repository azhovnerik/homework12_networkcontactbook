package services;

import entities.Contact;
import entities.TypeContact;
import entities.User;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import persistence.ContactRepository;
import exception.*;
import persistence.NetworkContactRepository;
import utils.validator.Validator;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RequiredArgsConstructor
@Getter
public class ContactService {
    private final ContactRepository contactRepository;
    private final Validator<Contact> validator;


    public void addContact(Contact contact){
        //contact validation
        if (!validator.isValid(contact)){
            throw new ValidateException("Contact is not valid");
        }
        if (contactRepository.SearchByValue(contact.getValue()).isPresent()){
            throw new DuplicateContactException();
        }
        contactRepository.addContact(contact);
    }



    public void deleteContact(String value){
        contactRepository.SearchByValue(value)
                .orElseThrow(()->new ContactNotFoundException());
        contactRepository.deleteContact(value);
    }
    public List<Contact> searchByName(String namePart){
       return contactRepository.searchByName(namePart);
    }
    public List<Contact> SearchByStartOfContact(String valueStart){
        return contactRepository.SearchByStartOfContact(valueStart);
    }
    public List<Contact> SelectContactsWithSorting(){
        return contactRepository.SelectContactsWithSorting();
    }
    public List<Contact> SelectByEmail(){
        return contactRepository.SelectByType(TypeContact.EMAIL);
    }

    public List<Contact> SelectByPhone(){
        return contactRepository.SelectByType(TypeContact.PHONE);
    }
    public Optional<Contact> SearchByValue(String value){
        return contactRepository.SearchByValue(value);
    }

    public List<User> SelectAllUsers() {
        //NetworkContactRepository networkContactRepository = (NetworkContactRepository)contactRepository; //делаем кастинг чтобы обратиться к специфичным для NetworkContactRepository методам
         return contactRepository.getAllUsers();
    }


}
