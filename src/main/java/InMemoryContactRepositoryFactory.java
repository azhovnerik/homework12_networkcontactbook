import persistence.CachedContactRepository;
import persistence.ContactRepository;
import persistence.InMemoryContactRepository;
import services.AuthService;
import services.InMemoryAuthService;

public class InMemoryContactRepositoryFactory implements ContactRepositoryFactory {
    @Override
    public ContactRepository createContactRepositort() {

        return new CachedContactRepository(new InMemoryContactRepository());
    }

    @Override
    public AuthService createAuthService() {
        return new InMemoryAuthService();
    }
}
