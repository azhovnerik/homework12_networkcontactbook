package utils.validator.contact;

import entities.Contact;
import entities.TypeContact;
import utils.validator.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneValidator implements Validator<Contact> {
    private static final Pattern PHONE_PATTERN = Pattern.compile("(\\+380|80|0)(\\d{9})\\b");
    @Override
    public boolean isValid(Contact contact) {
        if (contact.getType()!= TypeContact.PHONE){
            return true;
        }else{
            return PHONE_PATTERN.matcher(contact.getValue()).matches();

        }

    }
}
