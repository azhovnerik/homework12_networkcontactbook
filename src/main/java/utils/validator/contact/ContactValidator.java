package utils.validator.contact;

import entities.Contact;
import lombok.RequiredArgsConstructor;
import utils.validator.Validator;

import java.util.List;

@RequiredArgsConstructor
public class ContactValidator implements Validator<Contact> {
    private final List<Validator<Contact>> validators;
    @Override
    public boolean isValid(Contact contact) {
        return validators.stream().allMatch(v->v.isValid(contact));
    }
}
