package utils.validator.contact;

import entities.Contact;
import entities.TypeContact;
import utils.validator.Validator;

import java.util.regex.Pattern;

public class EmailValidator implements Validator<Contact> {
    private static final Pattern EMAIL_PATTERN = Pattern.compile("([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}\\b");

        @Override
        public boolean isValid(Contact contact) {
            if (contact.getType()!= TypeContact.EMAIL){
                return true;
            }else{
                return EMAIL_PATTERN.matcher(contact.getValue()).matches();

            }

        }
}

