import persistence.ContactRepository;
import services.AuthService;

public interface ContactRepositoryFactory {
    ContactRepository createContactRepositort();
    AuthService createAuthService();


}
