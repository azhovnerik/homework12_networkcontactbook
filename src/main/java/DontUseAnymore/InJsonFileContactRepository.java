/*
package DontUseAnymore;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import entities.Contact;
import entities.TypeContact;
import entities.User;
import persistence.ContactRepository;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;


public class InJsonFileContactRepository implements ContactRepository {
    List<Contact> contactList = new ArrayList<>();
    Gson gson = new GsonBuilder()
            .serializeNulls()
            .create();
    private OutputStream os;  // =

    public InJsonFileContactRepository() {
        readContactListFromFile(); //read existing contacts from file when object is created
    }

    @Override
    public void addContact(Contact contact) {
        //readContactListFromFile();
        //add new contact to contactList
        contact.setUuid(UUID.randomUUID().toString());
        contactList.add(contact);
        writeContactListToFile();
    }

    private void writeContactListToFile() {

        try (PrintWriter pw = new PrintWriter(new FileWriter("contactListJSON.txt"))) {
            pw.write(gson.toJson(contactList));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //read contacts from file to ArrayList contactList
    private void readContactListFromFile() {
        contactList.clear();
        //read contactList from file
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("contactListJSON.txt"))) {
            String json = bufferedReader.readLine();
            contactList = gson.fromJson(json, new TypeToken<ArrayList<Contact>>(){}.getType());

            }
         catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void deleteContact(String value) {
        List<Contact> cList = contactList.stream().filter(o -> o.getValue().equals(value)).collect(Collectors.toList());
        for (Contact cnt : cList
        ) {
            //  System.out.println("удаляется контакт: "+cnt);
            contactList.remove(cnt);
        }
        writeContactListToFile();
    }

    @Override
    public List<Contact> searchByName(String namePart) {
        return contactList.stream().filter(o -> (o.getSurname() + " " + o.getName() + " " + o.getPatronomic()).indexOf(namePart) != -1).collect(Collectors.toList());
    }

    @Override
    public List<Contact> SearchByStartOfContact(String valueStart) {
        return contactList.stream().filter(o -> o.getValue().startsWith(valueStart)).collect(Collectors.toList());

    }

    @Override
    public List<Contact> SelectContactsWithSorting() {

        return contactList.stream()
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> SelectByType(TypeContact type) {
        return contactList.stream()
                .filter(o -> o.getType() == type)
                .collect(Collectors.toList());

    }

    @Override
    public Optional<Contact> SearchByValue(String value) {
        return contactList.stream()
                .filter(o -> o.getValue()
                        .equals(value))
                .findFirst();
    }
    @Override
    public List<User> getAllUsers() {
        throw new UnsupportedOperationException();
    }
}
*/
