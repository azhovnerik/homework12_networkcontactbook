/*
package DontUseAnymore;

import entities.Contact;
import entities.TypeContact;
import persistence.ContactRepository;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;


public class InByteSerializeFileContactRepository implements ContactRepository {
    List<Contact> contactList = new ArrayList<>();

    //  private OutputStream os;  // =

    public InByteSerializeFileContactRepository() {
        readContactListFromFile2(); //read existing contacts from file when object is created
    }

    @Override
    public void addContact(Contact contact) {
        //readContactListFromFile();
        //add new contact to contactList
        contact.setUuid(UUID.randomUUID().toString());
        contactList.add(contact);
        writeContactListToFile2();
    }

    private void writeContactListToFile() {
        //String contactListString = contactList.stream().map(cnt -> cnt.toString()).collect(Collectors.joining("\r\n"));
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("contactListByteSerialize.dat"))) {
            os.writeInt(contactList.size());
            for (Contact contact : contactList) {
                os.writeObject(contact);
            }
            os.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //write ArrayList as is
    private void writeContactListToFile2() {
        //String contactListString = contactList.stream().map(cnt -> cnt.toString()).collect(Collectors.joining("\r\n"));
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("contactListByteSerialize.dat"))) {
            os.writeObject(contactList);
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //read contacts from file to ArrayList contactList
    private void readContactListFromFile() {
        contactList.clear();
        //read contactList from file
        try(FileInputStream f = new FileInputStream("contactListByteSerialize.dat");
        ObjectInputStream is = new ObjectInputStream(f)) {

            int size = is.readInt();
            for (int i = 0; i < size; i++) {
                try {
                    Contact contact = (Contact) is.readObject();
                    contactList.add(contact);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //read ArrayList as is
    private void readContactListFromFile2() {
        contactList.clear();
        //read contactList from file
        try(FileInputStream f = new FileInputStream("contactListByteSerialize.dat");
            ObjectInputStream is = new ObjectInputStream(f)) {
                try {
                     contactList = (List<Contact>) is.readObject();

                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void deleteContact(String value) {
        List<Contact> cList = contactList.stream().filter(o -> o.getValue().equals(value)).collect(Collectors.toList());
        for (Contact cnt : cList
        ) {
            //  System.out.println("удаляется контакт: "+cnt);
            contactList.remove(cnt);
        }
        writeContactListToFile();
    }

    @Override
    public List<Contact> searchByName(String namePart) {
        return contactList.stream().filter(o -> (o.getSurname() + " " + o.getName() + " " + o.getPatronomic()).indexOf(namePart) != -1).collect(Collectors.toList());
    }

    @Override
    public List<Contact> SearchByStartOfContact(String valueStart) {
        return contactList.stream().filter(o -> o.getValue().startsWith(valueStart)).collect(Collectors.toList());

    }

    @Override
    public List<Contact> SelectContactsWithSorting() {

        return contactList.stream()
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> SelectByType(TypeContact type) {
        return contactList.stream()
                .filter(o -> o.getType() == type)
                .collect(Collectors.toList());

    }

    @Override
    public Optional<Contact> SearchByValue(String value) {
        return contactList.stream()
                .filter(o -> o.getValue()
                        .equals(value))
                .findFirst();
    }
}
*/
