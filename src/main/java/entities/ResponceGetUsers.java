package entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
@Getter
public class ResponceGetUsers {
    private String status;
    List<User> users;
}
