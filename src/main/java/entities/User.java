package entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
//@AllArgsConstructor

//@RequiredArgsConstructor

@Data
@JsonIgnoreProperties({"password"})
public class User {
    private  String login;
    private  String date_born;
    private  String password;

    public User() {

    }

    public User(String login, String date_born, String password) {
        this.login = login;
        this.date_born = date_born;
        this.password = password;
    }
    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    // private final String password;


}
