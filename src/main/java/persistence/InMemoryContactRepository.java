package persistence;

import entities.Contact;
import entities.TypeContact;
import entities.User;
import persistence.ContactRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


public class InMemoryContactRepository implements ContactRepository {
    List<Contact> contactList = new ArrayList<>();

    @Override
    public void addContact(Contact contact) {
        contact.setUuid(UUID.randomUUID().toString());
        contactList.add(contact);
    }

    @Override
    public void deleteContact(String value) {
        List<Contact> cList =  contactList.stream().filter(o->o.getValue().equals(value)).collect(Collectors.toList());
        for (Contact cnt:cList
        ) {
          //  System.out.println("удаляется контакт: "+cnt);
            contactList.remove(cnt);
        }
    }

    @Override
    public List<Contact> searchByName(String namePart) {
        return   contactList.stream().filter(o->(o.getSurname()+" "+o.getName()+" "+o.getPatronomic()).indexOf(namePart)!=-1).collect(Collectors.toList());
    }

    @Override
    public List<Contact> SearchByStartOfContact(String valueStart) {
        return  contactList.stream().filter(o->o.getValue().startsWith(valueStart)).collect(Collectors.toList());

    }

    @Override
    public List<Contact> SelectContactsWithSorting() {
      return contactList.stream()
               .sorted(Contact.bySurnameComparator())
               .collect(Collectors.toList());
    }

    @Override
    public List<Contact> SelectByType(TypeContact type) {
        return  contactList.stream()
                .filter(o->o.getType()== type)
                .collect(Collectors.toList());

    }

    @Override
    public Optional<Contact> SearchByValue(String value) {
          return  contactList.stream()
                  .filter(o->o.getValue()
                  .equals(value))
                  .findFirst();
    }

    @Override
    public List<User> getAllUsers() {
        throw new UnsupportedOperationException();
    }
}
