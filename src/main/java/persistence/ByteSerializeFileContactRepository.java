package persistence;

import entities.Contact;
import entities.User;
import lombok.RequiredArgsConstructor;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class ByteSerializeFileContactRepository extends AbstractFileContactsRepository {
    private final String fileName;
    @Override
    protected List<Contact> readAll() {
        List<Contact> contactList = new ArrayList<>();
        //read contactList from file
        try(FileInputStream f = new FileInputStream(fileName);
            ObjectInputStream is = new ObjectInputStream(f)) {

            int size = is.readInt();
            for (int i = 0; i < size; i++) {
                try {
                    Contact contact = (Contact) is.readObject();
                    contactList.add(contact);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
            return  new ArrayList<>();
        }
        return  contactList;
    }

    @Override
    protected void saveAll(List<Contact> contacts) {
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(fileName))) {
            os.writeInt(contacts.size());
            for (Contact contact : contacts) {
                os.writeObject(contact);
            }
            os.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> getAllUsers() {
        throw new UnsupportedOperationException();
    }
}
