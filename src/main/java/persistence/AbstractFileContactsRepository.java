package persistence;

import entities.Contact;
import entities.TypeContact;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public abstract class AbstractFileContactsRepository implements ContactRepository {

    protected abstract List<Contact> readAll();
    protected abstract void saveAll(List<Contact> contacts);


    @Override
    public void addContact(Contact contact) {
         List<Contact> contacts = readAll();
         contact.setUuid(UUID.randomUUID().toString());
         contacts.add(contact);
         saveAll(contacts);
    }

    @Override
    public void deleteContact(String value) {
        List<Contact> toSave = readAll().stream()
                .filter(c->!c.getValue().equals(value))
                .collect(Collectors.toList());
         saveAll(toSave);

    }

    @Override
    public List<Contact> searchByName(String namePart) {
        return readAll().stream().filter(c-> Objects.nonNull(c.getValue()))
                .filter(c->c.getFullName().contains(namePart))
                .collect(Collectors.toList());

    }

    @Override
    public List<Contact> SearchByStartOfContact(String valueStart) {
        return readAll().stream().filter(c-> Objects.nonNull(c.getValue()))
                .filter(c->c.getValue().startsWith(valueStart))
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> SelectContactsWithSorting() {
        return readAll();
    }

    @Override
    public List<Contact> SelectByType(TypeContact type) {
        return readAll().stream().filter(c->c.getType()==type).collect(Collectors.toList());
    }

    @Override
    public Optional<Contact> SearchByValue(String value) {
        return readAll().stream()
                .filter(c-> Objects.nonNull(c.getValue()))
                .filter(c->c.getValue().equals(value))
                .findFirst();
    }
}
