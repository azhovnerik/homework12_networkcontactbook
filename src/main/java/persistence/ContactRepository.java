package persistence;

import entities.Contact;
import entities.TypeContact;
import entities.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public interface ContactRepository {
   void addContact(Contact contact);
   void deleteContact(String value);
   List<Contact> searchByName(String namePart);
   List<Contact> SearchByStartOfContact(String valueStart);
   List<Contact> SelectContactsWithSorting();
   List<Contact> SelectByType(TypeContact type);
   Optional<Contact> SearchByValue(String value);
   List<User> getAllUsers();
}

